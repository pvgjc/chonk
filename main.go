package main

import (
    "bytes"
    "embed"
    "fmt"
    "image"
    "image/color"
    _ "image/png"
    "io/fs"
    "log"
    "math/rand"
    "os"
    "strings"
    "time"
    
    "golang.org/x/image/font"
    "golang.org/x/image/font/opentype"

    "github.com/hajimehoshi/ebiten/v2"
    "github.com/hajimehoshi/ebiten/v2/text"
)

func init() {
    rand.Seed(time.Now().UnixNano())
}

var timelimit, _ = time.ParseDuration("2m")

// DANGER: DO NOT REMOVE THE "go:embed" COMMENTS!

//go:embed eina.ttf
var einattf []byte

var eina, _ = opentype.Parse(einattf)
var einafont, _ = opentype.NewFace(eina, &opentype.FaceOptions{Size:60,DPI:72,Hinting:font.HintingFull})

//go:embed assets/*
var assets embed.FS

var imgs = make(map[string]*ebiten.Image)

func init() {
    fs.WalkDir(assets, "assets", func(path string, d fs.DirEntry, err error) error {
        if !d.IsDir() {
            png, _ := assets.ReadFile(path)
            img, _, _ := image.Decode(bytes.NewReader(png))
            name := path[strings.LastIndex(path,"/")+1:]
            name = name[:strings.Index(name,".")]
            imgs[name] = ebiten.NewImageFromImage(img)
        }
        return nil
    })
}

type Game struct {  // GLOBAL VARIABLES:
    screen string   // current screen
    time time.Time  // time of completion
    x16 int         // x coordinate
    y16 int         // y coordinate
    xv16 int        // x velocity
    yv16 int        // y velocity
    count int       // game frame counter, used with aframe
    aframe int      // which frame chonk's sprite is on
    score int       // out of 720. used to draw the chonkometer
    tally int       // actual final score. used for the leaderboard
    flip bool       // whether we should flip chonk's sprite
    isEating bool   // whether chonk is currently eating
    cutscene bool   // for transitioning between levels
    staticObjects map[string][4]int // map of objects and their coordinates
    animatedObjects map[string][2]int // last minute hack :)
}

func NewGame() ebiten.Game { // set up Game on first launch
    g := &Game{}
    g.init()
    return g
}

func (g *Game) init() { // initialize global variables
    g.screen = "title"
    g.x16 = 800 * 16
}

var rescan = true
var leaderboard string

func (g *Game) read() string {
    if rescan {
        read, _ := os.ReadFile("/tmp/leaderboard")
        leaderboard = string(read)
        rescan = false
    }
    return leaderboard
}

func (g *Game) collide() int {
    if g.y16 < 0 {
        return 0 // stop getting stuck in the ceiling
    }
    b, m, w := 0, 0, 0
    for x := g.x16/16 + 80; x < g.x16/16 + 140; x++ {
        for y := g.y16/16 + 90; y > g.y16/16 + 60; y-- {
            switch imgs["map" + g.screen[3:4]].At(x,y) {
                case color.RGBA{0x00,0x00,0x00,0xff}: // black
                    b++
                case color.RGBA{0x85,0x85,0x85,0xff}: // gray
                    m++
                case color.RGBA{0xff,0xff,0xff,0xff}: // white
                    w++
            }
        }
    }
    if b > m && b > 3 {
        return 1 // ground
    } else if m > b && m > 40 {
        return 2 // bouncy 
    } else {
        return 0 // air
    }
}

func (g *Game) touching(coords [4]int) bool {
    for x := g.x16/16; x < g.x16/16 + 180; x++ {
        if x > coords[0] && x < coords[2] {
            for y := g.y16/16; y < g.y16/16 + 90; y++ {
                if y > coords[1] && y < coords[3] {
                    return true
                }
            }
        }
    }
    return false
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
    return 1920, 1080 // ah yes, nice crispy 1080p
}

func (g *Game) Update() error {
    if strings.HasPrefix(g.screen, "act") { // only process game during actual levels
        // BEGIN physics
        if !g.isEating { // freeze while eating, cartoon style
            if ebiten.IsKeyPressed(ebiten.KeyLeft) {
                g.xv16 -= 16 * 2
            }
            if ebiten.IsKeyPressed(ebiten.KeyRight) {
                g.xv16 += 16 * 2
            }
            g.xv16 = int(float64(g.xv16)*0.9) // x velocity decay, also softcaps velocity
            g.x16 += g.xv16
            n := 0 // simple slope detection
            for g.collide() > 0 && n < 5 {
                g.y16 -= 16
                n++
            } // and wall detection
            if g.collide() > 0 && n == 5 {
                g.x16 -= g.xv16
                g.y16 += 16 * 5
            }
            g.yv16 += 16 // gravity
            g.y16 += g.yv16
            if g.collide() > 0 && g.yv16 > 16 * -10 { // floor/ceiling detection
                g.y16 += g.yv16 * -1
                g.yv16 = 0
            }
            if g.collide() > 0 && g.yv16 <= 16* -10 { // reduce speed when passing through platforms
                g.yv16 -=5
            }
            g.y16 += 16 // nudge into the floor to check...
            if g.collide() > 0 { // if we're standing on solid ground...
                if ebiten.IsKeyPressed(ebiten.KeyUp) { // so we can jump
                    if g.collide() > 1 { // bouncy
                        g.yv16 = 16 * -40
                    } else {
                        g.yv16 = 16 * -25
                    }
                }
            }
            g.y16 -= 16 // end nudge
            // clamp x and y values to screen
            g.x16 = max(g.x16, 0)
            g.x16 = min(g.x16, 1740*16)
            g.y16 = max(g.y16, -90*16)
            g.y16 = min(g.y16, 990*16)
        }
        // END physics
        // BEGIN animations
        if g.xv16 > 0 { // set facing direction
            g.flip = true
        } else if g.xv16 < 0 {
            g.flip = false
        } // we don't check zero, so it only flips when the direction changes
        g.count++
        if g.count == 8 { // every 8 rendered frames...
            g.count = 0
            g.aframe++ // go to the next animation frame.
            if g.aframe == 6 {
                g.isEating = false // eating only lasts one cycle
                if g.cutscene {
                    switch g.screen {
                        case "act3":
                            g.tally += int(g.time.Sub(time.Now()).Seconds()*10)
                            rescan = true
                            g.screen = "credits"
                        case "act2":
                            g.screen = "act3" // TODO: add car, bird, and two planes
                            g.staticObjects = map[string][4]int{"bush":{960,800,1170,970},"lamp":{380,620,580,930},"tree":{1500,610,1750,950}}
                            g.animatedObjects = map[string][2]int{"car":{-300,750},"bird1":{-300,100},"plane":{2100,200}}
                        case "act1":
                            g.screen = "act2"
                            g.staticObjects = map[string][4]int{"plant3":{1200,260,1320,420},"shirt1":{730,570,900,760},"shirt2":{830,230,1050,460},"sweater":{990,580,1210,770}}
                            g.animatedObjects = map[string][2]int{"bird1":{-300,100},/*"guy":{2100,200},*/"bird2":{2100,200}}
                    }
                    g.x16 = 800*16
                    g.y16 = 0
                    g.score = 0
                    g.cutscene = false
                }
                g.aframe = 0 // loop back to the start
            }
        }
        // END animations
        if ebiten.IsKeyPressed(ebiten.KeySpace) { // eating time. this probably should have been its own function but too late now
            if !g.isEating {
                g.isEating = true
                g.aframe = 0
                g.count = 4 // start midway through the first frame. feels more responsive
                for object, coords := range g.staticObjects { // test all the static objects for eaten-ness
                    if g.touching(coords) {
                        g.score += 103 // seven objects per level
                        g.tally += 50
                        delete(g.staticObjects, object)
                        if len(g.staticObjects) == 0 && len(g.animatedObjects) == 0 {
                            g.cutscene = true
                        }
                        break // only eat one at a time!
                    }
                }
                switch (g.screen) { // now the animated ones
                    case "act1":
                        coords, present := g.animatedObjects["mouse"]
                        if present {
                            if g.touching([4]int{coords[0],coords[1],coords[0]+100,coords[1]+75}) {
                                g.score += 103 // seven objects per level
                                g.tally += 50
                                delete(g.animatedObjects, "mouse")
                                if len(g.staticObjects) == 0 && len(g.animatedObjects) == 0 {
                                    g.cutscene = true
                                }
                            }
                        }
                    case "act2":
                        coords, present := g.animatedObjects["bird1"]
                        if present {
                            if g.touching([4]int{coords[0],coords[1],coords[0]+250,coords[1]+200}) {
                                g.score += 103 // seven objects per level
                                g.tally += 100 // double because the guy is broken rn
                                delete(g.animatedObjects, "bird1")
                            }
                        } else {
                            /*coords, present := g.animatedObjects["guy"]
                            if present {
                                if g.touching([4]int{coords[0],coords[1],coords[0]+220,coords[1]+660}) {
                                    g.score += 103 // seven objects per level
                                    g.tally += 50
                                    delete(g.animatedObjects, "guy")
                                }
                            } else {*/
                            coords, present := g.animatedObjects["bird2"]
                            if present {
                                if g.touching([4]int{coords[0],coords[1],coords[0]+250,coords[1]+220}) {
                                    g.score += 103 // seven objects per level
                                    g.tally += 50
                                    delete(g.animatedObjects, "bird2")
                                    if len(g.staticObjects) == 0 && len(g.animatedObjects) == 0 {
                                        g.cutscene = true
                                    }
                                }
                            }
                            //}
                        }
                    case "act3":
                        coords, present := g.animatedObjects["car"]
                        if present {
                            if g.touching([4]int{coords[0],coords[1],coords[0]+720,coords[1]+360}) {
                                g.score += 103 // seven objects per level
                                g.tally += 50
                                delete(g.animatedObjects, "car")
                            }
                        } else {
                            coords, present := g.animatedObjects["bird1"]
                            if present {
                                if g.touching([4]int{coords[0],coords[1],coords[0]+250,coords[1]+200}) {
                                    g.score += 103 // seven objects per level
                                    g.tally += 50
                                    delete(g.animatedObjects, "bird1")
                                }
                            } else {
                                coords, present := g.animatedObjects["plane"]
                                if present {
                                    if g.touching([4]int{coords[0],coords[1],coords[0]+228,coords[1]+68}) {
                                        g.score += 206 // seven objects per level
                                        g.tally += 100 // plane worth double
                                        delete(g.animatedObjects, "plane")
                                        if len(g.staticObjects) == 0 && len(g.animatedObjects) == 0 {
                                            g.cutscene = true
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }
        if len(g.animatedObjects) != 0 { // move animated objects on tracks!
            switch (g.screen) {
                case "act1":
                    coords, present := g.animatedObjects["mouse"]
                    if present {
                        coords[0] -= 2
                        if coords[0] < -100 {
                            coords[0] = 2000
                        }
                        g.animatedObjects["mouse"] = coords
                    }
                case "act2":
                    coords, present := g.animatedObjects["bird1"]
                    if present {
                        coords[0] += 4
                        if coords[0] > 2000 {
                            coords[0] = -250
                        }
                        g.animatedObjects["bird1"] = coords
                    } else {
                        /*coords, present := g.animatedObjects["guy"]
                        if present {
                            if coords[1] == 200 {
                                coords[0] -= 2
                            }
                            if coords[0] < 1700 {
                                coords[1] = 201
                            }
                            if coords[1] == 201 {
                                coords[0] += 2
                            }
                            if coords[0] > 2100 {
                                coords[1] = 200
                            }
                            g.animatedObjects["guy"] = coords
                        } else {*/
                        coords, present := g.animatedObjects["bird2"]
                        if present {
                            coords[0] -= 5
                            if coords[0] < -250 {
                                coords[0] = 2000
                            }
                            g.animatedObjects["bird2"] = coords
                        }
                        //}
                    }
                case "act3":
                    coords, present := g.animatedObjects["car"]
                    if present {
                        coords[0] += 8
                        if coords[0] > 2000 {
                            coords[0] = -700
                        }
                        g.animatedObjects["car"] = coords
                    } else {
                        coords, present := g.animatedObjects["bird1"]
                        if present {
                            coords[0] += 10
                            if coords[0] > 2000 {
                                coords[0] = -250
                            }
                            g.animatedObjects["bird1"] = coords
                        } else {
                        coords, present := g.animatedObjects["plane"]
                        if present {
                            coords[0] -= 15
                            if coords[0] < -228 {
                                coords[0] = 2000
                            }
                            g.animatedObjects["plane"] = coords
                        }
                    }
                }
            }
        }
        if !g.cutscene && g.time.Sub(time.Now()).Seconds() < 0 { // running out of time boots you to the credits
            rescan = true
            g.screen = "credits"
        }
    } else if g.screen == "title" {
        if ebiten.IsKeyPressed(ebiten.KeyT) && ebiten.IsKeyPressed(ebiten.KeySpace) {
            g.screen = "act1"
            g.staticObjects = map[string][4]int{"books1":{130,320,380,450},"books2":{150,110,400,270},"food":{1790,930,1910,1020},"pillows":{1160,620,1390,820},"plant1":{1400,270,1530,450},"plant2":{450,500,590,1010}}
            g.animatedObjects = map[string][2]int{"mouse":{2500,900}}
            g.x16 = 800*16
            g.y16 = 0
            g.score = 0
            // global reset stuff
            g.tally = 0
            g.time = time.Now().Add(timelimit)
        }
    } else if g.screen == "credits" {
        if ebiten.IsKeyPressed(ebiten.KeyEscape) {
            rescan = true
            g.screen = "title"
        }
    }
    
    return nil // don't panic!
}

func (g *Game) Draw(screen *ebiten.Image) {
    if g.cutscene {
        screen.DrawImage(imgs["transition"], &ebiten.DrawImageOptions{})
    } else {
        screen.DrawImage(imgs[g.screen], &ebiten.DrawImageOptions{})
    }
    if strings.HasPrefix(g.screen, "act") {
        g.drawChonk(screen)
        for object, _ := range g.staticObjects {
            screen.DrawImage(imgs[object], &ebiten.DrawImageOptions{})
        }
        g.drawAnimatedObject(screen)
        g.drawChonkOMeter(screen)
        text.Draw(screen, fmt.Sprintf("%0.1f", g.time.Sub(time.Now()).Seconds()), einafont, 900, 100, color.RGBA{0xff,0xff,0xff,0xff})
    } else if g.screen == "title" {
        text.Draw(screen, g.read(), einafont, 1170, 250, color.RGBA{0x00,0x00,0x00,0xff})
    } else if g.screen == "credits" {
        text.Draw(screen, g.read(), einafont, 1200, 450, color.RGBA{0x00,0x00,0x00,0xff})
        text.Draw(screen, fmt.Sprintf("Your score: %d", g.tally*17), einafont, 300, 1000, color.RGBA{0xff,0xff,0xff,0xff})
    }
}

func (g *Game) drawChonk(screen *ebiten.Image) {
    op := &ebiten.DrawImageOptions{}
    if g.flip {
        op.GeoM.Scale(-1,1) // the actual flip
        op.GeoM.Translate(float64(g.x16/16.0)+180,float64(g.y16/16.0)) // offset so image stays in bounds
    } else {
        op.GeoM.Translate(float64(g.x16/16.0),float64(g.y16/16.0)) // draw image at current coordinates
    }
    if g.isEating { // eat animation takes priority
        screen.DrawImage(imgs["eat"].SubImage(image.Rect(0,90*g.aframe,180,90*g.aframe+90)).(*ebiten.Image), op)
    } else if g.xv16 == 0 { // only idle animation if standing still
        screen.DrawImage(imgs["idle"].SubImage(image.Rect(0,90*g.aframe,180,90*g.aframe+90)).(*ebiten.Image), op)
    } else { // take 90-pixel slice of image corresponding to current frame of animation
        screen.DrawImage(imgs["run"].SubImage(image.Rect(0,90*g.aframe,180,90*g.aframe+90)).(*ebiten.Image), op)
    }
}

func (g *Game) drawAnimatedObject(screen *ebiten.Image) {
    if len(g.animatedObjects) == 0 {
        return
    }
    op := &ebiten.DrawImageOptions{}
    switch (g.screen) {
        case "act1":
            coords, present := g.animatedObjects["mouse"]
            if present {
                op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                screen.DrawImage(imgs["mouse"].SubImage(image.Rect(0,75*g.aframe,100,75*g.aframe+75)).(*ebiten.Image), op)
            }
        case "act2":
            coords, present := g.animatedObjects["bird1"]
            if present {
                op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                screen.DrawImage(imgs["bird1"].SubImage(image.Rect(0,200*g.aframe,250,200*g.aframe+200)).(*ebiten.Image), op)
            } else {
                /*coords, present := g.animatedObjects["guy"]
                if present {
                    op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                    screen.DrawImage(imgs["guy"].SubImage(image.Rect(0,660*g.aframe,220,660*g.aframe+660)).(*ebiten.Image), op)
                } else {*/
                coords, present := g.animatedObjects["bird2"]
                if present {
                    op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                    screen.DrawImage(imgs["bird2"].SubImage(image.Rect(0,200*g.aframe,250,200*g.aframe+200)).(*ebiten.Image), op)
                }
                //}
            }
        case "act3":
            coords, present := g.animatedObjects["car"]
            if present {
                op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                screen.DrawImage(imgs["car"].SubImage(image.Rect(0,360*g.aframe,720,360*g.aframe+360)).(*ebiten.Image), op)
            } else {
                coords, present := g.animatedObjects["bird1"]
                if present {
                    op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                    screen.DrawImage(imgs["bird1"].SubImage(image.Rect(0,200*g.aframe,250,200*g.aframe+200)).(*ebiten.Image), op)
                } else {
                    coords, present := g.animatedObjects["plane"]
                    if present {
                        op.GeoM.Translate(float64(coords[0]),float64(coords[1]))
                        screen.DrawImage(imgs["plane"].SubImage(image.Rect(0,68*g.aframe,228,68*g.aframe+68)).(*ebiten.Image), op)
                    }
                }
            }
    }
}

func (g *Game) drawChonkOMeter(screen *ebiten.Image) {
    screen.DrawImage(imgs["empty"].SubImage(image.Rect(0,0,288,100+(720-g.score))).(*ebiten.Image), &ebiten.DrawImageOptions{})
    op := &ebiten.DrawImageOptions{}
    op.GeoM.Translate(0,float64(100+(720-g.score)))
    screen.DrawImage(imgs["full"].SubImage(image.Rect(0,100+(720-g.score),288,1080)).(*ebiten.Image), op)
}

func main() {
    ebiten.SetWindowSize(1280, 720) // if fullscreen unsupported or disabled
    ebiten.SetFullscreen(true)
    ebiten.SetWindowTitle("Chonk")
    if err := ebiten.RunGame(NewGame()); err != nil {
        log.Fatal(err)
    }
}
